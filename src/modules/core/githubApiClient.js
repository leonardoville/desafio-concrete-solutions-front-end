import Github from 'github-api';

const githubApiClient = new Github({ token: process.env.REACT_APP_GITHUB_API_TOKEN });

export default githubApiClient;
