export const FETCH_START = 'FETCH_START';
export const FETCH_FINISH = 'FETCH_FINISH';
export const CHANGE_PAGE_TITLE = 'CHANGE_PAGE_TITLE';
export const CLEAR_ERROR = 'CLEAR_ERROR';
export const FETCH_ERROR = 'FETCH_ERROR';
