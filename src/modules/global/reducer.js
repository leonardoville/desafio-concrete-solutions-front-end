import { FETCH_START, FETCH_FINISH, FETCH_ERROR, CHANGE_PAGE_TITLE, CLEAR_ERROR } from "./actionTypes";

const INITIAL_STATE = {
  fetchingData: false,
  error: {},
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch(type) {
    case CHANGE_PAGE_TITLE:
      return {
        ...state,
        currentPageTitle: payload,
      };
    case FETCH_START:
      return {
        ...state,
        fetchingData: true,
      };
    case FETCH_FINISH:
      return {
        ...state,
        fetchingData: false,
      };
    case FETCH_ERROR:
      return {
        ...state,
        fetchingData: false,
        error: { message: payload },
      };
    case CLEAR_ERROR:
      return {
        ...state,
        error: {},
      };
    default:
      return state;
  }
};
