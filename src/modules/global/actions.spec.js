import Chance from 'chance';

import { CHANGE_PAGE_TITLE, CLEAR_ERROR, FETCH_ERROR, FETCH_FINISH, FETCH_START } from './actionTypes';
import { changePageTitle, clearError, fetchError, fetchFinish, fetchStart } from './actions';

describe('GlobalActionCreator', () => {
  let chance;

  beforeAll(() => {
    chance = new Chance();
  });

  it('should create an action to change page title', () => {
    const pageTitleStub = chance.word();
    const expectedAction = {
      type: CHANGE_PAGE_TITLE,
      payload: pageTitleStub,
    };

    expect(changePageTitle(pageTitleStub)).toEqual(expectedAction);
  });

  it('should create an action to fetch start', () => {
    const expectedAction = {
      type: FETCH_START,
    };

    expect(fetchStart()).toEqual(expectedAction);
  });

  it('should create an action to fetch finish', () => {
    const expectedAction = {
      type: FETCH_FINISH,
    };

    expect(fetchFinish()).toEqual(expectedAction);
  });

  it('should create an action to fetch error', () => {
    const errorMessageStub = chance.sentence();
    const expectedAction = {
      type: FETCH_ERROR,
      payload: errorMessageStub,
    };

    expect(fetchError(errorMessageStub)).toEqual(expectedAction);
  });

  it('should create an action to clear error', () => {
    const expectedAction = {
      type: CLEAR_ERROR,
    };

    expect(clearError()).toEqual(expectedAction);
  });
});
