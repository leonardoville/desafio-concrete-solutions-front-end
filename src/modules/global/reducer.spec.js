import Chance from 'chance';

import { CHANGE_PAGE_TITLE, FETCH_START, FETCH_FINISH, FETCH_ERROR, CLEAR_ERROR } from "./actionTypes";
import reducer from './reducer';

describe('GlobalReducer', () => {
  let chance;

  beforeAll(() => {
    chance = new Chance();
  });

  it('should return the initialState', () => {
    expect(reducer(undefined, {})).toEqual({ fetchingData: false, error: {} })
  });

  it(`should handle ${CHANGE_PAGE_TITLE}`, () => {
    const pageTitleStub = chance.word();
    const action = { type: CHANGE_PAGE_TITLE, payload: pageTitleStub };

    expect(reducer({}, action)).toEqual({ currentPageTitle: pageTitleStub });
  });

  it(`should handle ${FETCH_START}`, () => {
    const action = { type: FETCH_START };

    expect(reducer({}, action)).toEqual({ fetchingData: true });
  });

  it(`should handle ${FETCH_FINISH}`, () => {
    const action = { type: FETCH_FINISH };

    expect(reducer({}, action)).toEqual({ fetchingData: false });
  });

  it(`should handle ${FETCH_ERROR}`, () => {
    const errorMessageStub = chance.sentence();
    const action = { type: FETCH_ERROR, payload: errorMessageStub };
    const expected = { fetchingData: false, error: { message: errorMessageStub } };

    expect(reducer({ fetchingData: true }, action)).toEqual(expected);
  });

  it(`should handle ${CLEAR_ERROR}`, () => {
    const action = { type: CLEAR_ERROR };

    expect(reducer({ error: { message: {} } }, action)).toEqual({ error: {} });
  });
 });
