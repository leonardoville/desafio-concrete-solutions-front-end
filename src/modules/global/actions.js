import { CHANGE_PAGE_TITLE, FETCH_FINISH, FETCH_START, FETCH_ERROR, CLEAR_ERROR } from './actionTypes';

export function fetchStart() {
  return {
    type: FETCH_START,
  };
}

export function fetchFinish() {
  return {
    type: FETCH_FINISH,
  };
}

export function fetchError(errorMessage) {
  return {
    type: FETCH_ERROR,
    payload: errorMessage,
  };
}

export function changePageTitle(title) {
  return {
    type: CHANGE_PAGE_TITLE,
    payload: title,
  };
}

export function clearError() {
  return {
    type: CLEAR_ERROR,
  };
}
