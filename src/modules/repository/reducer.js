import { FETCH_REPOSITORY_DETAILS } from './actionTypes';
import { Repository } from './model';

export default (state = {}, { type, payload }) => {
  switch(type) {
    case FETCH_REPOSITORY_DETAILS:
      return new Repository(payload);
    default:
      return state;
  }
};
