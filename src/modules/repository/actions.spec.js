import Chance from 'chance';
import configureStore from 'redux-mock-store';

import { LOCATION_CHANGE } from 'react-router-redux';
import thunk from 'redux-thunk';

import global from '../global';
import { FETCH_REPOSITORY_DETAILS } from './actionTypes';

describe('RepositoryActions', () => {
  let chance;

  beforeAll(() => {
    chance = new Chance();
  });

  beforeEach(() => {
    jest.unmock('../core');
  });

  it('creates actions when get user github repository by name and owner has been done', async () => {
    const mockStore = configureStore([thunk]);
    const store = mockStore({});

    const repoNameStub = chance.name();
    const repoOwnerStub = chance.name();
    const repositoryDataStub = { name: chance.name() };

    const getRepoDetailsMock = jest.fn(() => ({ data: repositoryDataStub }));
    const getRepoMock = jest.fn((repoOwner, repoName) => ({ getDetails: getRepoDetailsMock }));

    jest.doMock('../core', () => ({ GithubApiClient: { getRepo: getRepoMock } }));

    const actions = require('./actions');
    await store.dispatch(actions.fetchRepositoryDetails(repoOwnerStub, repoNameStub));

    expect(getRepoMock).toBeCalledWith(repoOwnerStub, repoNameStub);
    expect(getRepoDetailsMock).toBeCalled();

    expect(store.getActions()).toEqual([
      { type: global.actionTypes.FETCH_START },
      { type: FETCH_REPOSITORY_DETAILS, payload: repositoryDataStub },
      { type: global.actionTypes.FETCH_FINISH },
    ]);
  });

  it('create FETCH_ERROR action when fetchRepositoryDetails call throws a error', async () => {
    const mockStore = configureStore([thunk]);
    const store = mockStore({});

    const repoOwnerStub = chance.name();
    const repoNameStub = chance.name();

    jest.resetModules();
    jest.doMock('../core', () => ({ GithubApiClient: { getUser: () => Promise.reject() } }));

    const actions = require('./actions');
    await store.dispatch(actions.fetchRepositoryDetails(repoOwnerStub, repoNameStub));

    expect(store.getActions()).toEqual([
      { type: global.actionTypes.FETCH_START },
      { type: global.actionTypes.FETCH_ERROR, payload: 'Repositório pesquisado, não foi encontrado!' },
    ]);
  });
});
