import { GithubApiClient }from '../core';
import global from '../global';

import { FETCH_REPOSITORY_DETAILS } from './actionTypes';

const handleFetchRepositoryDetails = async(owner, name, dispatch) => {
  await dispatch(global.actions.fetchStart());

  const repository = await GithubApiClient.getRepo(owner, name);
  const response = await repository.getDetails();

  await dispatch({ type: FETCH_REPOSITORY_DETAILS, payload: response.data });
  await dispatch(global.actions.fetchFinish());
};

export const fetchRepositoryDetails = (owner, name) => async dispatch => {
  try {
    await handleFetchRepositoryDetails(owner, name, dispatch);
  } catch (e) {
    await dispatch(global.actions.fetchError('Repositório pesquisado, não foi encontrado!'));
  }
};
