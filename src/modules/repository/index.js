import * as containers from './containers';
import * as constants from './constants'
import reducer from './reducer';

export default {
  containers,
  constants,
  reducer,
}
