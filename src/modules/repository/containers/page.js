import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { isEmpty } from 'lodash';

import global from '../../global';

import { fetchRepositoryDetails } from '../actions';
import { REPOSITORY_DETAILS_PAGE_TITLE } from '../constants';
import { Details, GithubLink } from '../components';

import './page.css';

export class RepositoryDetails extends React.Component {
  componentDidMount() {
    const { owner, name } = this.props.params;
    this.props.fetchRepository(owner, name);
  }

  componentWillMount() {
    const { params: { owner, name }, changePageTitle } = this.props;

    changePageTitle(`${REPOSITORY_DETAILS_PAGE_TITLE}: ${owner}/${name}`);
  }

  componentWillReceiveProps({ params: { owner, name } }) {
    if (this.props.params.owner !== owner && this.props.params.name !== name)
      this.props.changePageTitle(`${REPOSITORY_DETAILS_PAGE_TITLE}: ${owner}/${name}`);
  }

  renderContent() {
    return (
      <React.Fragment>
        <Details {...this.props.repository} />
        <div className="github-link-icon-wrapper">
          <GithubLink href={this.props.repository.externalLinkGithub} />
        </div>
      </React.Fragment>
    );
  }

  render() {
    return (
      <div>
        {isEmpty(this.props.repository) ? null : this.renderContent()}
      </div>
    );
  };
}

function mapStateToProps(state, ownProps) {
  return {
    repository: state.repository,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchRepository: fetchRepositoryDetails,
    changePageTitle: global.actions.changePageTitle,
  }, dispatch)
}

RepositoryDetails.propTypes = {
  params: PropTypes.shape({
    owner: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }),
  fetchRepository: PropTypes.func.isRequired,
  repository: PropTypes.object.isRequired,
  changePageTitle: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(RepositoryDetails);
