import React from 'react';
import { shallow } from 'enzyme';
import Chance from 'chance';

import { RepositoryDetails } from './page';

import { REPOSITORY_DETAILS_PAGE_TITLE } from '../constants';

describe('RepositoryDetailsPage', () => {
  let chance;

  beforeAll(() => {
    chance = new Chance();
  });

  const buildRepository = () => ({
    name: chance.name(),
    description: chance.sentence(),
    stargazersCount: chance.natural(),
    language: chance.word(),
    externalLinkGithub: chance.url(),
  });

  it('should render RepositoryDetailsPage component with correct structure', () => {
    const wrapper1 = shallow(
      <RepositoryDetails
        repository={{}}
        fetchRepository={jest.fn}
        changePageTitle={jest.fn}
        params={{ owner: 'teste repo owner', name: 'teste repo' }}
      />
    );

    expect(wrapper1).toMatchSnapshot();
  });

  it('should fetch repository details', () => {
    const fetchRepositoryDetailsMock = jest.fn();
    const changePageTitleMock = jest.fn();

    const repoName = chance.name();
    const repoOwner = chance.name();

    shallow(
      <RepositoryDetails
        repository={buildRepository()}
        fetchRepository={fetchRepositoryDetailsMock}
        changePageTitle={changePageTitleMock}
        params={{ owner: repoOwner, name: repoName }} />
    );

    expect(changePageTitleMock).toBeCalledWith(`${REPOSITORY_DETAILS_PAGE_TITLE}: ${repoOwner}/${repoName}`);
    expect(fetchRepositoryDetailsMock).toBeCalledWith(repoOwner, repoName);
  });
});
