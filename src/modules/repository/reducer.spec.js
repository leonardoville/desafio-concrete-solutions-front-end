import Chance from 'chance';
import { omit } from 'lodash';

import reducer from './reducer';

import { FETCH_REPOSITORY_DETAILS } from './actionTypes';

describe('RepositoryReducer', () => {
  let chance;

  beforeAll(() => {
    chance = new Chance();
  });

  it('should return the initialState', () => {
    expect(reducer(undefined, {})).toEqual({})
  });

  it(`should handle ${FETCH_REPOSITORY_DETAILS}`, () => {
    const payload = {
      name: chance.name(),
      description: chance.sentence(),
      stargazers_count: chance.natural(),
      language: chance.word(),
      html_url: chance.url(),
    };

    const expected = {
      stargazersCount: payload.stargazers_count,
      externalLinkGithub: payload.html_url,
      ...omit(payload, ['stargazers_count', 'html_url'])
    };
    const action = { type: FETCH_REPOSITORY_DETAILS, payload };

    expect(reducer({}, action)).toEqual(expected);
  });
});
