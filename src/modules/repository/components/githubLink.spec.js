import React from 'react';
import { shallow } from 'enzyme';

import GithubLink from './githubLink';

describe('GithubLink', () => {
  it('should render GithubLink component with correct structure', () => {
    const wrapper1 = shallow(<GithubLink href="teste" />);

    expect(wrapper1).toMatchSnapshot();
  });
});
