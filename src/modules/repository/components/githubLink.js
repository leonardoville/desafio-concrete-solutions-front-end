import React from 'react';
import PropTypes from 'prop-types';

import RaisedButton from 'material-ui/RaisedButton';

const GithubLink = ({ href }) => (
  <RaisedButton
    href={href}
    target="_blank"
    primary={true}
    label="Github Link"
  />
);

GithubLink.propTypes = {
  href: PropTypes.string.isRequired,
};

export default GithubLink;
