import React from 'react';
import { shallow } from 'enzyme';

import Details from './details';

describe('Details', () => {
  it('should render Details component with correct structure', () => {
    const props1 = {
      name: 'teste',
      description: 'teste desc',
      stargazersCount: 2,
      language: 'java',
    };

    const wrapper1 = shallow(
      <Details {...props1} />
    );

    expect(wrapper1).toMatchSnapshot();


    const props2 = {
      name: 'teste2',
      description: 'teste desc asc',
      stargazersCount: 2000,
      language: 'js',
    };

    const wrapper2 = shallow(
      <Details {...props2} />
    );

    expect(wrapper2).toMatchSnapshot();
  });
});
