import React from 'react';
import PropTypes from 'prop-types';

import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';

const Details = ({ name, description, stargazersCount, language }) => (
  <div>
    <TextField
      readOnly
      id="name"
      value={name}
      floatingLabelFixed={true}
      underlineShow={false}
      floatingLabelText="Nome" />
    <Divider />
    <TextField
      readOnly
      value={language}
      floatingLabelFixed={true}
      underlineShow={false}
      floatingLabelText="Linguagem" />
    <Divider />
    <TextField
      readOnly
      value={stargazersCount}
      type="number"
      floatingLabelFixed={true}
      underlineShow={false}
      floatingLabelText="Estrelas" />
    <Divider />
    <TextField
      readOnly
      value={description}
      floatingLabelFixed={true}
      multiLine={true}
      rows={5}
      fullWidth={true}
      floatingLabelText="Descrição" />
  </div>
);

Details.propTypes = {
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  stargazersCount: PropTypes.number.isRequired,
  language: PropTypes.string.isRequired,
};

export default Details;
