export class Repository {
  name;
  description;
  stargazersCount;
  language;
  externalLinkGithub;

  constructor({ name, description, stargazers_count, language, html_url }) {
    this.name = name;
    this.description = description || '';
    this.stargazersCount = stargazers_count;
    this.language = language || '';
    this.externalLinkGithub = html_url;
  }
}
