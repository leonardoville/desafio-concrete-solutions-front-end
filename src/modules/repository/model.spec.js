import { Repository } from './model';

describe('Model', () => {
  it('Repository without data', () => {
    const profile = new Repository({});

    expect(profile).toBeInstanceOf(Repository);
    expect(profile).toMatchObject({
      name: undefined,
      description: '',
      stargazersCount: undefined,
      language: '',
      externalLinkGithub: undefined,
    })
  });
});
