import { createStore } from 'redux';

import userReducer from './reducer';

import find from './find';

describe('UserReducer', () => {
  it('should combineReducers with keys defined in constants of each reducer module and same initial state', () => {
    let store = createStore(userReducer);

    expect(store.getState()[find.constants.NAME]).toEqual(find.reducer(undefined, {}));
  });
});
