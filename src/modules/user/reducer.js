import { combineReducers } from 'redux';

import find from './find';
import details from './details';

export default combineReducers({
  [find.constants.NAME]: find.reducer,
  [details.constants.NAME]: details.reducer,
});
