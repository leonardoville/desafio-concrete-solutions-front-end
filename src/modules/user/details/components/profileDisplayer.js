import React from 'react';
import PropTypes from 'prop-types';

import Avatar from 'material-ui/Avatar';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';

const ProfileDisplayer = ({ avatar, followers, following, email, bio }) => (
  <div>
    <div style={style.avatarWrapper}>
      <Avatar style={style.avatar} src={avatar}/>
    </div>
    <TextField
      readOnly
      id="followers"
      value={followers}
      floatingLabelFixed={true}
      type="number"
      underlineShow={false}
      floatingLabelText="Seguidores" />
    <Divider />
    <TextField
      readOnly
      value={following}
      type="number"
      floatingLabelFixed={true}
      underlineShow={false}
      floatingLabelText="Seguindo" />
    <Divider />
    <TextField
      readOnly
      value={email}
      floatingLabelFixed={true}
      underlineShow={false}
      floatingLabelText="Email" />
    <Divider />
    <TextField
      readOnly
      value={bio}
      floatingLabelFixed={true}
      multiLine={true}
      rows={2}
      fullWidth={true}
      floatingLabelText="Bio" />
  </div>
);

const style = {
  avatarWrapper: {
    textAlign: 'center',
  },
  avatar: {
    height: '10rem',
    width: '10rem',
  },
};

ProfileDisplayer.propTypes = {
  profile: PropTypes.shape({
    avatar: PropTypes.string.isRequired,
    followers: PropTypes.number.isRequired,
    following: PropTypes.number.isRequired,
    email: PropTypes.string.isRequired,
    bio: PropTypes.string.isRequired,
  })
};

export default ProfileDisplayer;
