import React from 'react';
import { shallow } from 'enzyme';

import ProfileDisplayer from './profileDisplayer';

describe('ProfileDisplayer', () => {
  it('should render SortableHeaderColumn component with correct structure', () => {
    const profile = {
      avatar: 'http://www.gravatar.com/avatar/d1b2a730b496df7ed8d23cc5fe528395',
      followers: 5587282828460032,
      following: 6631502666268672,
      email: 'ek@gutoci.ml',
      bio: 'zawhahem',
    };

    const wrapper = shallow(
      <ProfileDisplayer profile={profile} />
    );

    expect(wrapper).toMatchSnapshot();
  });
});
