import React from 'react';
import PropTypes from 'prop-types';

import FontIcon from 'material-ui/FontIcon';
import { grey600 } from 'material-ui/styles/colors'

const style = {
  cursor: 'pointer',
  marginRight: '2.5%',
};

const RepositoriesTableActions = ({ showRepositoryDetails }) => (
  <React.Fragment>
    <FontIcon className="material-icons" onClick={showRepositoryDetails} style={style} color={grey600}>
      search
    </FontIcon>
  </React.Fragment>
);

RepositoriesTableActions.propTypes = {
  showRepositoryDetails: PropTypes.func.isRequired,
};

export default RepositoriesTableActions;
