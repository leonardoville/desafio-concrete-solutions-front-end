import React from 'react';
import PropTypes from 'prop-types';

import { TableHeader, TableHeaderColumn, TableRow } from 'material-ui/Table';

import SortableHeaderColumn from './sort/headerColumn';

const RepositoriesTableHeader = ({ sortDirection, sortRepositoriesByStars }) => (
  <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
    <TableRow>
      <TableHeaderColumn>Id</TableHeaderColumn>
      <TableHeaderColumn>Nome</TableHeaderColumn>
      <SortableHeaderColumn columnName="Estrelas" direction={sortDirection} onSort={sortRepositoriesByStars} />
      <TableHeaderColumn>Ações</TableHeaderColumn>
    </TableRow>
  </TableHeader>
);

RepositoriesTableHeader.propTypes = {
  sortDirection: PropTypes.string.isRequired,
  sortRepositoriesByStars: PropTypes.func.isRequired
};

RepositoriesTableHeader.muiName = 'TableHeader';

export default RepositoriesTableHeader;
