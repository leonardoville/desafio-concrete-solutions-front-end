import React from 'react';
import { shallow } from 'enzyme';

import RepositoriesTableRow from './row';

describe('RepositoriesTableRow', () => {
  it('should render RepositoriesTableRow component with correct structure', () => {
    const wrapper = shallow(
      <RepositoriesTableRow stargazersCount={400} fullName={'Teste'} id={100} showRepositoryDetails={jest.fn}/>
    );

    expect(wrapper).toMatchSnapshot();
  });
});
