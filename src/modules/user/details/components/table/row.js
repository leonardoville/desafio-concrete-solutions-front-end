import React from 'react';
import PropTypes from 'prop-types';

import { TableRow, TableRowColumn } from 'material-ui/Table'

import RepositoriesTableActions from './actions';

const style = {
  rowActions: {
    overflow: 'visible',
  },
};

const RepositoriesTableRow = ({ id, fullName, stargazersCount, showRepositoryDetails }) => (
  <TableRow>
    <TableRowColumn>{id}</TableRowColumn>
    <TableRowColumn>{fullName}</TableRowColumn>
    <TableRowColumn>{stargazersCount}</TableRowColumn>
    <TableRowColumn style={style.rowActions}>
      <RepositoriesTableActions showRepositoryDetails={() => showRepositoryDetails(fullName)}/>
    </TableRowColumn>
  </TableRow>
);

RepositoriesTableRow.propTypes = {
  id: PropTypes.number.isRequired,
  fullName: PropTypes.string.isRequired,
  stargazersCount: PropTypes.number.isRequired,
  showRepositoryDetails: PropTypes.func.isRequired,
};

export default RepositoriesTableRow;
