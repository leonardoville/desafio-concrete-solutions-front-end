import React from 'react';
import PropTypes from 'prop-types';

import { Table, TableBody } from 'material-ui/Table';

import RepositoriesTableHeader from './header';
import RepositoriesTableRow from './row';

const style = {
  table: {
    width: '100%',
    tableLayout: 'auto',
    overflow: 'auto',
  },
  tableBody: {
    overflowX: 'inherit',
  },
  tableWrapper: {
    overflowX: 'auto',
  },
};

const renderRows = (repositories, showRepositoryDetails) => (
  repositories.map(it => <RepositoriesTableRow {...it} showRepositoryDetails={showRepositoryDetails} key={it.id} />)
);

const RepositoriesTable = ({ repositories, sortDirection, sortRepositoriesByStars, showRepositoryDetails }) => (
  <Table
    selectable={false}
    fixedHeader={false}
    bodyStyle={style.tableBody}
    style={style.table}
    wrapperStyle={style.tableWrapper}>
    <RepositoriesTableHeader sortDirection={sortDirection} sortRepositoriesByStars={sortRepositoriesByStars} />
    <TableBody displayRowCheckbox={false}>
      {renderRows(repositories, showRepositoryDetails)}
    </TableBody>
  </Table>
);

RepositoriesTable.propTypes = {
  repositories: PropTypes.array.isRequired,
  sortDirection: PropTypes.string.isRequired,
  sortRepositoriesByStars: PropTypes.func.isRequired,
  showRepositoryDetails: PropTypes.func.isRequired,
};

export default RepositoriesTable;
