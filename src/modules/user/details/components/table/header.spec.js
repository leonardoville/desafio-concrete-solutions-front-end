import React from 'react';
import { shallow } from 'enzyme';

import RepositoriesTableHeader from './header';

describe('RepositoriesTableHeader', () => {
  it('should render RepositoriesTableHeader component with correct structure', () => {
    const wrapper1 = shallow(
      <RepositoriesTableHeader sortDirection={'ASC'} sortRepositoriesByStars={jest.fn}/>
    );

    expect(wrapper1).toMatchSnapshot();

    const wrapper2 = shallow(
      <RepositoriesTableHeader sortDirection={'DESC'} sortRepositoriesByStars={jest.fn}/>
    );

    expect(wrapper2).toMatchSnapshot();
  });
});
