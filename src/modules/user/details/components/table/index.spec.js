import React from 'react';
import { shallow } from 'enzyme';
import Chance from 'chance';

import RepositoriesTable from './index';

describe('RepositoriesTable', () => {
  let chance;

  beforeAll(() => {
    chance = new Chance();
  });

  it('should render RepositoriesTable component with correct structure', () => {
    const repositories1 = [
      {
        id: 5620133665964032,
        fullName: 'Nathan Ball',
        stargazersCount: 5776037640142848,
        showRepositoryDetails: jest.fn(),
      },
      {
        id: 5536510128422912,
        fullName: 'Adeline Nelson',
        stargazersCount: 3146999880744960,
        showRepositoryDetails: jest.fn(),
      }
    ];

    const repositories2 = [
      {
        id: 553651012,
        fullName: 'Adeline Teste',
        stargazersCount: 314699944960,
        showRepositoryDetails: jest.fn(),
      }
    ];

    const wrapper1 = shallow(
      <RepositoriesTable
        repositories={repositories1}
        sortRepositoriesByStars={jest.fn}
        showRepositoryDetails={jest.fn}
        sortDirection={'ASC'} />
    );

    expect(wrapper1).toMatchSnapshot();

    const wrapper2 = shallow(
      <RepositoriesTable
        repositories={[...repositories1, ...repositories2]}
        showRepositoryDetails={jest.fn}
        sortRepositoriesByStars={jest.fn}
        sortDirection={'ASC'} />
    );

    expect(wrapper2).toMatchSnapshot();

    const wrapper3 = shallow(
      <RepositoriesTable
        repositories={[...repositories2]}
        showRepositoryDetails={jest.fn}
        sortRepositoriesByStars={jest.fn}
        sortDirection={'DESC'} />
    );

    expect(wrapper3).toMatchSnapshot();
  });
});
