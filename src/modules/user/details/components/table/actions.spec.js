import React from 'react';
import { shallow } from 'enzyme';

import RepositoriesTableActions from './actions';

describe('RepositoriesTableActions', () => {
  it('should render RepositoriesTableActions component with correct structure', () => {
    const wrapper = shallow(
      <RepositoriesTableActions showRepositoryDetails={jest.fn} />
    );

    expect(wrapper).toMatchSnapshot();
  });
});
