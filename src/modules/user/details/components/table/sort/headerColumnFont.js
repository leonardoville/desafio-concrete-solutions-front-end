import React from 'react';
import PropTypes from 'prop-types';

import FontIcon from 'material-ui/FontIcon';
import { grey600 } from 'material-ui/styles/colors'

import { SORT_ASC } from '../../../model';

const iconStyle = {
  cursor: 'pointer',
  verticalAlign: 'middle',
};

const SortableHeaderColumnFont = ({ direction }) => (
  <FontIcon className="material-icons" style={iconStyle} color={grey600}>
    { direction === SORT_ASC ? 'keyboard_arrow_up' : 'keyboard_arrow_down'}
  </FontIcon>
);

SortableHeaderColumnFont.propTypes = {
  direction: PropTypes.string.isRequired,
};

export default SortableHeaderColumnFont;
