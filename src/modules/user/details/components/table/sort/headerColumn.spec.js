import React from 'react';
import { shallow } from 'enzyme';

import SortableHeaderColumn from './headerColumn';

describe('SortableHeaderColumn', () => {
  it('should render SortableHeaderColumn component with correct structure', () => {
    const wrapper = shallow(
      <SortableHeaderColumn direction={'ASC'} onSort={jest.fn} columnName={'Teste'}/>
    );

    expect(wrapper).toMatchSnapshot();
  });
});
