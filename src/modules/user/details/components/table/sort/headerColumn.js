import React from 'react';
import PropTypes from 'prop-types';

import { TableHeaderColumn } from 'material-ui/Table';

import SortableHeaderColumnFont from './headerColumnFont';

const SortableHeaderColumn = ({ columnName, onSort, direction }) => (
  <TableHeaderColumn>
    <div onClick={onSort}>
      {columnName}
      <SortableHeaderColumnFont direction={direction} />
    </div>
  </TableHeaderColumn>
);

SortableHeaderColumn.propTypes = {
  direction: PropTypes.string.isRequired,
  onSort: PropTypes.func.isRequired,
  columnName: PropTypes.string.isRequired,
};

export default SortableHeaderColumn;
