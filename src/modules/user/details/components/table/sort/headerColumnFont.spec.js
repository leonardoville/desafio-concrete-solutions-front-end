import React from 'react';
import { shallow, mount } from 'enzyme';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import SortableHeaderColumnFont from './headerColumnFont';

describe('SortableHeaderColumnFont', () => {
  it('should render SortableHeaderColumnFont component with correct structure', () => {
    const wrapper = shallow(
      <SortableHeaderColumnFont direction={'ASC'} />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('should render icon "keyboard_arrow_up" for sort direction "ASC"', () => {
    const wrapper = mount(<MuiThemeProvider><SortableHeaderColumnFont direction={'ASC'} /></MuiThemeProvider>);

    expect(wrapper.text()).toBe('keyboard_arrow_up');
  });

  it('should render icon "keyboard_arrow_down" for sort direction "DESC"', () => {
    const wrapper = mount(<MuiThemeProvider><SortableHeaderColumnFont direction={'DESC'} /></MuiThemeProvider>);

    expect(wrapper.text()).toBe('keyboard_arrow_down');
  });
});
