export class Repository {
  id;
  fullName;
  stargazersCount;

  constructor({ id, full_name, stargazers_count }) {
    this.id = id;
    this.fullName = full_name;
    this.stargazersCount = stargazers_count;
  }
}

export const SORT_ASC = 'ASC';
export const SORT_DESC = 'DESC';
