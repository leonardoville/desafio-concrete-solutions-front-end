import Chance from 'chance';
import { sortBy } from 'lodash';

import reducer from './reducer';
import { FETCH_USER_REPOS, SORT_REPOSITORIES_BY_STARS } from './actionTypes';
import { SORT_DESC, SORT_ASC } from './model';

describe('DetailsUserReducer', () => {
  let chance;

  beforeAll(() => {
    chance = new Chance();
  });

  it('should return the initialState', () => {
    expect(reducer(undefined, {})).toEqual({ sortDirection: SORT_DESC, repositories: [] })
  });

  it(`should handle ${FETCH_USER_REPOS}`, () => {
    const repositoryId = chance.natural();
    const repositoryFullName = chance.name();
    const stargazersCount = chance.natural();

    const expected = {
      repositories: [
        {
          id: repositoryId,
          fullName: repositoryFullName,
          stargazersCount: stargazersCount,
        },
      ],
    };

    const payload = [{ id: repositoryId, full_name: repositoryFullName, stargazers_count: stargazersCount }];
    const action = { type: FETCH_USER_REPOS, payload };

    expect(reducer({}, action)).toEqual(expected);
  });

  it(`should handle ${SORT_REPOSITORIES_BY_STARS} and sort ASC stargazersCount`, () => {
    const repository1 = {
      id: chance.natural(),
      stargazersCount: chance.natural(),
    };
    const repository2 = {
      id: chance.natural(),
      stargazersCount: chance.natural(),
    };
    const repository3 = {
      id: chance.natural(),
      stargazersCount: chance.natural(),
    };

    const initalState = {
      sortDirection: SORT_DESC,
      repositories: [
        {
          id: repository1.id,
          stargazersCount: repository1.stargazersCount,
        },
        {
          id: repository2.id,
          stargazersCount: repository2.stargazersCount,
        },
        {
          id: repository3.id,
          stargazersCount: repository3.stargazersCount,
        },
      ],
    };

    const sortedStubs = sortBy([repository1, repository2, repository3], 'stargazersCount');

    const expected = {
      sortDirection: SORT_ASC,
      repositories: [
        {
          id: sortedStubs[0].id,
          stargazersCount: sortedStubs[0].stargazersCount,
        },
        {
          id: sortedStubs[1].id,
          stargazersCount: sortedStubs[1].stargazersCount,
        },
        {
          id: sortedStubs[2].id,
          stargazersCount: sortedStubs[2].stargazersCount,
        },
      ],
    };

    const action = { type: SORT_REPOSITORIES_BY_STARS };

    expect(reducer(initalState, action)).toEqual(expected);
  });

  it(`should handle ${SORT_REPOSITORIES_BY_STARS} and sort DESC stargazersCount`, () => {
    const repository1 = {
      id: chance.natural(),
      stargazersCount: chance.natural(),
    };
    const repository2 = {
      id: chance.natural(),
      stargazersCount: chance.natural(),
    };
    const repository3 = {
      id: chance.natural(),
      stargazersCount: chance.natural(),
    };

    const initalState = {
      sortDirection: SORT_ASC,
      repositories: [
        {
          id: repository1.id,
          stargazersCount: repository1.stargazersCount,
        },
        {
          id: repository2.id,
          stargazersCount: repository2.stargazersCount,
        },
        {
          id: repository3.id,
          stargazersCount: repository3.stargazersCount,
        },
      ],
    };

    const sortedStubs = sortBy([repository1, repository2, repository3], 'stargazersCount').reverse();

    const expected = {
      sortDirection: SORT_DESC,
      repositories: [
        {
          id: sortedStubs[0].id,
          stargazersCount: sortedStubs[0].stargazersCount,
        },
        {
          id: sortedStubs[1].id,
          stargazersCount: sortedStubs[1].stargazersCount,
        },
        {
          id: sortedStubs[2].id,
          stargazersCount: sortedStubs[2].stargazersCount,
        },
      ],
    };

    const action = { type: SORT_REPOSITORIES_BY_STARS };

    expect(reducer(initalState, action)).toEqual(expected);
  });

  it(`should handle ${SORT_REPOSITORIES_BY_STARS} and sort stargazersCount by direction in payload of action`, () => {
    const repository1 = {
      id: chance.natural(),
      stargazersCount: chance.natural(),
    };
    const repository2 = {
      id: chance.natural(),
      stargazersCount: chance.natural(),
    };
    const repository3 = {
      id: chance.natural(),
      stargazersCount: chance.natural(),
    };

    const initalState = {
      sortDirection: SORT_DESC,
      repositories: [
        {
          id: repository1.id,
          stargazersCount: repository1.stargazersCount,
        },
        {
          id: repository2.id,
          stargazersCount: repository2.stargazersCount,
        },
        {
          id: repository3.id,
          stargazersCount: repository3.stargazersCount,
        },
      ],
    };

    const sortedStubs = sortBy([repository1, repository2, repository3], 'stargazersCount').reverse();

    const expected = {
      sortDirection: SORT_DESC,
      repositories: [
        {
          id: sortedStubs[0].id,
          stargazersCount: sortedStubs[0].stargazersCount,
        },
        {
          id: sortedStubs[1].id,
          stargazersCount: sortedStubs[1].stargazersCount,
        },
        {
          id: sortedStubs[2].id,
          stargazersCount: sortedStubs[2].stargazersCount,
        },
      ],
    };

    const action = { type: SORT_REPOSITORIES_BY_STARS, payload: SORT_DESC };

    expect(reducer(initalState, action)).toEqual(expected);
  });
});
