import * as constants from './constants';
import * as containers from './containers';
import * as actions from './actions';
import reducer from './reducer';

export default {
  constants,
  containers,
  actions,
  reducer,
};
