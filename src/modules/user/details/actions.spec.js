import Chance from 'chance';
import configureStore from 'redux-mock-store';

import { LOCATION_CHANGE } from 'react-router-redux';
import thunk from 'redux-thunk';

import { SORT_DESC } from './model';
import { SORT_REPOSITORIES_BY_STARS, FETCH_USER_REPOS } from './actionTypes';

jest.mock('react-router-redux', () => ({
  ...require.requireActual('react-router-redux'),
  push: jest.fn(route => ({ type: require('react-router-redux')['LOCATION_CHANGE'] }))
}));

describe('DetailsUserActions', () => {
  let chance;

  beforeAll(() => {
    chance = new Chance();
  });

  it('create FETCH_USER_REPOS when get user github list repositories has been done', async () => {
    const mockStore = configureStore([thunk]);
    const store = mockStore({});

    const searchUsernameStub = chance.name();
    const repositoriesStub = [];

    const getUserRepositoriesMock = jest.fn(() => ({ data: repositoriesStub }));
    const getUserMock = jest.fn(() => ({ listRepos: getUserRepositoriesMock }));

    jest.doMock('../../core', () => ({ GithubApiClient: { getUser: getUserMock } }));

    const actions = require('./actions');
    await store.dispatch(actions.fetchUserRepos(searchUsernameStub));

    expect(getUserMock).toBeCalled();
    expect(getUserRepositoriesMock).toBeCalled();

    expect(store.getActions()).toEqual([
      { type: FETCH_USER_REPOS, payload: repositoriesStub },
      { type: SORT_REPOSITORIES_BY_STARS, payload: SORT_DESC },
    ]);
  });

  it('create SORT_REPOSITORIES_BY_STARS action', () => {
    const mockStore = configureStore([thunk]);
    const store = mockStore({});

    const direction = chance.word();

    const actions = require('./actions');
    store.dispatch(actions.sortRepositoriesByStars(direction));

    expect(store.getActions()).toEqual([
      { type: SORT_REPOSITORIES_BY_STARS, payload: direction },
    ]);
  });

  it('create SHOW_REPOSITORY_DETAILS action', async () => {
    const mockStore = configureStore([thunk]);
    const store = mockStore({});

    const ownerStub = chance.name();
    const repoStub = chance.name();

    const reactRouterRedux = require('react-router-redux');
    const actions = require('./actions');
    await store.dispatch(actions.showRepositoryDetails(`${ownerStub}/${repoStub}`));

    expect(reactRouterRedux.push).toBeCalledWith(`/repositories/${ownerStub}/${repoStub}`);
    expect(store.getActions()).toEqual([
      { type: LOCATION_CHANGE },
    ]);
  });
});
