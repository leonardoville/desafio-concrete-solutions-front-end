import { push } from 'react-router-redux';

import { GithubApiClient } from '../../core'

import {
  FETCH_USER_REPOS,
  SORT_REPOSITORIES_BY_STARS,
} from './actionTypes';
import { SORT_DESC } from './model';

export const fetchUserRepos = (name) => async dispatch => {
  const user = await GithubApiClient.getUser(name);
  const response = await user.listRepos();

  await dispatch({ type: FETCH_USER_REPOS, payload: response.data });
  await dispatch(sortRepositoriesByStars(SORT_DESC));
};

export const sortRepositoriesByStars = (direction) => {
  return { type: SORT_REPOSITORIES_BY_STARS, payload: direction }
};

export const showRepositoryDetails = (fullName) => async dispatch => {
  const [owner, name] = fullName.split('/');

  await dispatch(push(`/repositories/${owner}/${name}`));
};
