import { sortBy } from 'lodash';

import { FETCH_USER_REPOS, SORT_REPOSITORIES_BY_STARS } from './actionTypes';
import { Repository, SORT_ASC, SORT_DESC } from './model';

const sortRepositoriesByDirection = (repositories, direction) =>
  direction === SORT_ASC
    ? sortBy(repositories, 'stargazersCount')
    : sortBy(repositories, 'stargazersCount').reverse();

const buildUserRepositoriesState = payload =>
  payload.map(it => new Repository(it));

const INITIAL_STATE = {
  sortDirection: SORT_DESC,
  repositories: []
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch(type) {
    case SORT_REPOSITORIES_BY_STARS:
      const sortDirection = payload || (state.sortDirection === SORT_DESC ? SORT_ASC : SORT_DESC);

      return {
        sortDirection,
        repositories: sortRepositoriesByDirection(state.repositories, sortDirection),
      };
    case FETCH_USER_REPOS:
      return {
        ...state,
        repositories: buildUserRepositoriesState(payload)
      };
    default:
      return state;
  }
};
