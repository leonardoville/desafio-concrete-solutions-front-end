import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { isEmpty } from 'lodash';

import global from '../../../global';
import find from '../../find';

import { RepositoriesTable, ProfileDisplayer } from '../components';
import { USER_DETAILS_PAGE_TITLE } from '../constants'
import { sortRepositoriesByStars, showRepositoryDetails } from '../actions';

export class UserDetailsPage extends React.Component {
  componentDidMount() {
    if (isEmpty(this.props.profile) && isEmpty(this.props.repositories))
      this.props.fetchUser(this.props.params.username);
  }

  componentWillMount() {
    this.props.changePageTitle(`${USER_DETAILS_PAGE_TITLE}: ${this.props.params.username}`);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.params.username !== nextProps.params.username)
      this.props.changePageTitle(`${USER_DETAILS_PAGE_TITLE}: ${nextProps.params.username}`);
  }

  renderRepositoriesTable() {
    return (
      <RepositoriesTable
        sortRepositoriesByStars={this.props.sortRepositoriesByStars}
        sortDirection={this.props.sortDirection}
        repositories={this.props.repositories}
        showRepositoryDetails={this.props.showRepositoryDetails} />
    );
  }

  render() {
    return (
      <React.Fragment>
        {isEmpty(this.props.profile) ? null : <ProfileDisplayer {...this.props.profile} />}
        {isEmpty(this.props.repositories) ? null : this.renderRepositoriesTable()}
      </React.Fragment>
    )
  }
}

function mapStateToProps(state, ownProps) {
  return {
    profile: state.user.profile,
    repositories: state.user.details.repositories,
    sortDirection: state.user.details.sortDirection,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    changePageTitle: global.actions.changePageTitle,
    fetchUser: find.actions.searchUser,
    sortRepositoriesByStars: () => sortRepositoriesByStars(),
    showRepositoryDetails,
  }, dispatch);
}

UserDetailsPage.propTypes = {
  params: PropTypes.shape({ username: PropTypes.string.isRequired }).isRequired,
  profile: PropTypes.object.isRequired,
  repositories: PropTypes.array.isRequired,
  sortDirection: PropTypes.string.isRequired,
  changePageTitle: PropTypes.func.isRequired,
  fetchUser: PropTypes.func.isRequired,
  sortRepositoriesByStars: PropTypes.func.isRequired,
  showRepositoryDetails: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(UserDetailsPage);
