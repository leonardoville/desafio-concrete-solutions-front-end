import React from 'react';
import { shallow } from 'enzyme';
import Chance from 'chance';

import { UserDetailsPage } from './page';

import { USER_DETAILS_PAGE_TITLE } from '../constants'

describe('UserDetailsPage', () => {
  let chance;

  beforeAll(() => {
    chance = new Chance();
  });

  const buildProfile = () => ({
    following: chance.natural(),
    avatar: chance.avatar(),
    followers: chance.natural(),
    email: chance.email(),
    bio: chance.sentence(),
  });

  it('should render UserDetailsPage component with correct structure', () => {
    const wrapper1 = shallow(
      <UserDetailsPage
        sortRepositoriesByStars={jest.fn}
        changePageTitle={jest.fn}
        fetchUser={jest.fn}
        showRepositoryDetails={jest.fn}
        sortDirection={'ASC'}
        repositories={[]}
        profile={{}}
        params={{ username: 'Teste 1' }} />
    );

    expect(wrapper1).toMatchSnapshot();

    const wrapper2 = shallow(
      <UserDetailsPage
        sortRepositoriesByStars={jest.fn}
        changePageTitle={jest.fn}
        fetchUser={jest.fn}
        showRepositoryDetails={jest.fn}
        sortDirection={'DESC'}
        repositories={[{}]}
        profile={{}}
        params={{ username: 'Teste 2' }} />
    );

    expect(wrapper2).toMatchSnapshot();
  });

  it('should fetch user profile', () => {
    const fetchUserMock = jest.fn();
    const usernameStub = chance.name();

    const wrapper = shallow(
      <UserDetailsPage
        sortRepositoriesByStars={jest.fn}
        changePageTitle={jest.fn}
        fetchUser={fetchUserMock}
        showRepositoryDetails={jest.fn}
        sortDirection={chance.word()}
        repositories={[]}
        profile={{}}
        params={{ username: usernameStub }} />
    );

    wrapper.instance().componentDidMount();

    expect(fetchUserMock).toBeCalledWith(usernameStub);
  });

  it('should not fetch user profile', () => {
    const fetchUserMock = jest.fn();
    const usernameStub = chance.name();
    const profileStub = buildProfile();

    const wrapper = shallow(
      <UserDetailsPage
        sortRepositoriesByStars={jest.fn}
        changePageTitle={jest.fn}
        showRepositoryDetails={jest.fn}
        fetchUser={fetchUserMock}
        sortDirection={chance.word()}
        repositories={[]}
        profile={profileStub}
        params={{ username: usernameStub }} />
    );

    wrapper.instance().componentDidMount();

    expect(fetchUserMock).not.toBeCalled();
  });

  it('should invoke function changePageTitle', () => {
    const changePageTitleMock = jest.fn();
    const usernameStub = chance.name();
    const newUsernameStub = chance.name();
    const profileStub = buildProfile();

    const wrapper = shallow(
      <UserDetailsPage
        sortRepositoriesByStars={jest.fn}
        changePageTitle={changePageTitleMock}
        fetchUser={jest.fn}
        showRepositoryDetails={jest.fn}
        sortDirection={chance.word()}
        repositories={[]}
        profile={profileStub}
        params={{ username: usernameStub }} />
    );

    wrapper.instance().componentWillReceiveProps({ params: { username: newUsernameStub } });

    expect(changePageTitleMock).toBeCalledWith(`${USER_DETAILS_PAGE_TITLE}: ${newUsernameStub}`);
  });

  it('should not invoke function changePageTitle', () => {
    const changePageTitleMock = jest.fn();
    const newChangePageTitleMock = jest.fn();
    const usernameStub = chance.name();
    const profileStub = buildProfile();

    const wrapper = shallow(
      <UserDetailsPage
        sortRepositoriesByStars={jest.fn}
        changePageTitle={changePageTitleMock}
        fetchUser={jest.fn}
        showRepositoryDetails={jest.fn}
        sortDirection={chance.word()}
        repositories={[]}
        profile={profileStub}
        params={{ username: usernameStub }} />
    );

    wrapper.setProps({ changePageTitle: newChangePageTitleMock });

    expect(changePageTitleMock).toBeCalledWith(`${USER_DETAILS_PAGE_TITLE}: ${usernameStub}`);
    expect(newChangePageTitleMock).not.toBeCalled();
  });
});
