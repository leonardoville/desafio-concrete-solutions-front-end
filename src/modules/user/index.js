import details from './details';
import find from './find';

import * as constants from './constants';
import reducer from './reducer';

export default {
  reducer,
  constants,
  details,
  find,
};
