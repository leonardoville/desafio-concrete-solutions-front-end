import Chance from 'chance';
import configureStore from 'redux-mock-store';

import { LOCATION_CHANGE } from 'react-router-redux';
import thunk from 'redux-thunk';

import global from '../../global';
import { FETCH_USER_PROFILE } from './actionTypes';

describe('FindUserProfileActions', () => {
  let chance;

  beforeAll(() => {
    chance = new Chance();
  });

  beforeEach(() => {
    jest.unmock('react-router-redux');
    jest.unmock('../../core');
    jest.unmock('../details');
  });

  it('creates actions when get user github profile and repositories has been done', async () => {
    const mockStore = configureStore([thunk]);
    const store = mockStore({});

    const searchUsernameStub = chance.name();
    const profileDataStub = { name: chance.name() };
    const fetchUserReposActionType = chance.name();

    const getUserProfileMock = jest.fn(() => ({ data: profileDataStub }));
    const getUserMock = jest.fn((name) => ({ getProfile: getUserProfileMock }));
    const fetchUserRepos = jest.fn(() => ({ type: fetchUserReposActionType }));
    const pushMock = jest.fn(() => ({ type: LOCATION_CHANGE }));

    jest.doMock('react-router-redux', () => ({ push: pushMock }));
    jest.doMock('../../core', () => ({ GithubApiClient: { getUser: getUserMock } }));
    jest.doMock('../details', () => ({ actions: { fetchUserRepos } }));

    const actions = require('./actions');
    await store.dispatch(actions.searchUser(searchUsernameStub));

    expect(getUserMock).toBeCalledWith(searchUsernameStub);
    expect(getUserProfileMock).toBeCalled();
    expect(fetchUserRepos).toBeCalledWith(searchUsernameStub);
    expect(pushMock).toBeCalledWith(`/user/${searchUsernameStub}`);

    expect(store.getActions()).toEqual(expect.arrayContaining([
      { type: global.actionTypes.FETCH_START },
      { type: FETCH_USER_PROFILE, payload: profileDataStub },
      { type: fetchUserReposActionType },
      { type: global.actionTypes.FETCH_FINISH },
      { type: LOCATION_CHANGE },
    ]));
  });

  it('create FETCH_ERROR action when searchUser call throws a error', async () => {
    const mockStore = configureStore([thunk]);
    const store = mockStore({});

    const searchUsernameStub = chance.name();

    jest.resetModules();
    jest.doMock('../../core', () => ({ GithubApiClient: { getUser: () => Promise.reject() } }));

    const actions = require('./actions');
    await store.dispatch(actions.searchUser(searchUsernameStub));

    expect(store.getActions()).toEqual([
      { type: global.actionTypes.FETCH_START },
      { type: global.actionTypes.FETCH_ERROR, payload: 'Usuário pesquisado, não foi encontrado!' },
    ]);
  });
});
