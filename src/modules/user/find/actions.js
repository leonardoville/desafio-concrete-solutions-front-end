import { push } from 'react-router-redux';

import { FETCH_USER_PROFILE } from './actionTypes';
import global from '../../global';
import details from '../details';
import { GithubApiClient }from '../../core'

const fetchUserProfile = name => async dispatch => {
  const user = await GithubApiClient.getUser(name);
  const response = await user.getProfile();

  dispatch({ type: FETCH_USER_PROFILE, payload: response.data });
};

const handleSearchUser = async (name, dispatch) => {
  await dispatch(global.actions.fetchStart());

  await Promise.all([
    dispatch(fetchUserProfile(name)),
    dispatch(details.actions.fetchUserRepos(name))
  ]);

  await dispatch(global.actions.fetchFinish());

  await dispatch(push(`/user/${name}`));
};

export const searchUser = (name) => async dispatch => {
  try {
    await handleSearchUser(name, dispatch);
  } catch(e) {
    await dispatch(global.actions.fetchError('Usuário pesquisado, não foi encontrado!'));
  }
};
