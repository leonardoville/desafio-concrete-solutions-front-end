import Chance from 'chance';
import { omit } from 'lodash';

import reducer from './reducer';

import { FETCH_USER_PROFILE } from './actionTypes';

describe('FindUserProfileReducer', () => {
  let chance;

  beforeAll(() => {
    chance = new Chance();
  });

  it('should return the initialState', () => {
    expect(reducer(undefined, {})).toEqual({})
  });

  it(`should handle ${FETCH_USER_PROFILE}`, () => {
    const payload = {
      avatar_url: chance.avatar(),
      followers: chance.integer(),
      following: chance.integer(),
      bio: chance.sentence(),
      email: chance.email(),
    };

    const expected = { avatar: payload.avatar_url, ...omit(payload, 'avatar_url') };
    const action = { type: FETCH_USER_PROFILE, payload };

    expect(reducer({}, action)).toEqual(expected);
  });
});
