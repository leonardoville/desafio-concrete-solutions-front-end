import { FETCH_USER_PROFILE } from './actionTypes';

import { Profile } from './model';

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch(type) {
    case FETCH_USER_PROFILE:
      return {
        ...new Profile(payload),
      };
    default:
      return state;
  }
};
