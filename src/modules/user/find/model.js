export class Profile {
  avatar;
  followers;
  following;
  bio;
  email;

  constructor({ avatar_url, followers, following, bio, email }) {
    this.avatar = avatar_url;
    this.followers = followers || 0;
    this.following = following || 0;
    this.email = email || '';
    this.bio = bio || '';
  }
}
