import { Profile } from './model';

describe('Model', () => {
  it('Profile without data', () => {
    const profile = new Profile({});

    expect(profile).toBeInstanceOf(Profile);
    expect(profile).toMatchObject({
      avatar: undefined,
      followers: 0,
      following: 0,
      bio: '',
      email: '',
    })
  });
});
