import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import FontIcon from 'material-ui/FontIcon';
import { white } from 'material-ui/styles/colors'

import { searchUser } from '../actions';
import { SearchField } from '../components';

export class SearchUser extends React.Component {
  wrapperStyle = {
    position: 'relative',
    display: 'inline-block'
  };

  iconStyle = {
    position: 'absolute',
    left: 0,
    top: '35%',
    width: 20,
    height: 20
  };

  renderFontIcon() {
    return <FontIcon className="material-icons" style={this.iconStyle} color={white}>search</FontIcon>;
  }

  render() {
    return (
      <div style={Object.assign(this.wrapperStyle, this.props.wrapperFieldStyle)}>
        {this.renderFontIcon()}
        <SearchField searchUser={this.props.searchUser}/>
      </div>
    );
  }
}

SearchUser.propTypes = {
  searchUser: PropTypes.func.isRequired,
  wrapperFieldStyle: PropTypes.object,
};

SearchUser.defaultProps = {
  wrapperFieldStyle: {},
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ searchUser }, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchUser);
