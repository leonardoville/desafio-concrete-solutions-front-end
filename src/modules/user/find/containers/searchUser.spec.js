import React from 'react';
import { shallow } from 'enzyme';

import { SearchUser } from './searchUser';

describe('SearchUser', () => {
  it('should render SearchUser component with correct structure', () => {
    const wrapper = shallow(<SearchUser searchUser={jest.fn} wrapperFieldStyle={{}}/>);

    expect(wrapper).toMatchSnapshot();
  });
});
