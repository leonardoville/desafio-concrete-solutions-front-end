import React from 'react';
import { shallow, mount } from 'enzyme';
import Chance from 'chance';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';

import SearchUserField from './searchField';

import { debounce } from 'lodash';

jest.mock('lodash', () => ({
  ...require.requireActual('lodash'),
  debounce: jest.fn((fn, timeout) => event => fn(event, event.target.value)),
}));

describe('SearchUserField', () => {
  let chance;

  beforeAll(() => {
    chance = new Chance();
  });

  it('should render SearchField component with correct structure', () => {
    const wrapper = shallow(<SearchUserField searchUser={jest.fn} />);

    expect(wrapper).toMatchSnapshot();
  });

  it('should invoke debounce lodash function and searchUser when TextField value is changed', () => {
    const onSearchUserMock = jest.fn();
    const searchUserName = chance.name();
    const event = { target: { name: 'change', value: searchUserName } };

    const wrapper = mount(<MuiThemeProvider><SearchUserField searchUser={onSearchUserMock} /></MuiThemeProvider>);

    wrapper.find(TextField).find('input').simulate('change', event);

    expect(debounce).toBeCalledWith(wrapper.find(SearchUserField).instance().searchUser, 400);
    expect(onSearchUserMock).toBeCalledWith(searchUserName);
  });

  it('should not invoke searchUser function when TextField value is changed is blank', () => {
    const onSearchUserMock = jest.fn();
    const searchUserName = chance.name();
    const event = { target: { name: 'change', value: '' } };

    const wrapper = mount(<MuiThemeProvider><SearchUserField searchUser={onSearchUserMock} /></MuiThemeProvider>);

    wrapper.find(TextField).find('input').simulate('change', event);

    expect(onSearchUserMock).not.toBeCalledWith(searchUserName);
  });
});
