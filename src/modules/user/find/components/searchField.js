import React from 'react';
import PropTypes from 'prop-types';

import { debounce, isEmpty } from 'lodash';

import TextField from 'material-ui/TextField';

class SearchUserField extends React.Component {
  fieldStyle = {
    textIndent: 30,
    width: '100%',
  };

  inputStyle = {
    color: 'white',
  };

  fieldHintStyle = {
    color: 'white',
  };

  constructor(props) {
    super(props);
    this.searchUser = this.searchUser.bind(this);
  }

  searchUser(event, newValue) {
    if (!isEmpty(newValue))
      this.props.searchUser(newValue);
  }

  render() {
    return (
      <TextField
        style={this.fieldStyle}
        inputStyle={this.inputStyle}
        hintText="Busque pelo usuário"
        onChange={debounce(this.searchUser, 400)}
        hintStyle={this.fieldHintStyle}
      />
    );
  }
}

SearchUserField.propTypes = {
  searchUser: PropTypes.func.isRequired,
};

export default SearchUserField;
