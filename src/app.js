import React from 'react';
import PropTypes from 'prop-types';
import { Router, Route } from 'react-router';

import Layout from './components/layout';

import { user, repository } from './modules';

const App = (props) => (
  <Router history={props.history}>
    <Route path="/" component={Layout}>
      <Route path="/user/:username" component={user.details.containers.Page} />
      <Route path="/repositories/:owner/:name" component={repository.containers.Page} />
    </Route>
  </Router>
);

App.propTypes = {
  history: PropTypes.object.isRequired,
};

export default App;
