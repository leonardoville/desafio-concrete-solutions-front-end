import React from 'react';
import { shallow } from 'enzyme';

import NavBar from './index';

it('should render layout with title with success', () => {
  const wrapper = shallow(<NavBar />);
  expect(wrapper).toMatchSnapshot();
});
