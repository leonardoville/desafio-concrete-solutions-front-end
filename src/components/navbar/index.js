import React from 'react';
import AppBar from 'material-ui/AppBar';

import { user } from '../../modules';
const { containers: { SearchUser }} = user.find;

const styles = {
  bar: {
    textAlign: 'center',
    zIndex: 0,
  },
  title: {
    width: '60%',
  },
};

const NavBar = () => (
  <AppBar
    style={styles.bar}
    title={<SearchUser wrapperFieldStyle={styles.title} />}
    iconElementLeft={<span />}
  />
);

export default NavBar;
