import React from 'react';

import CircularProgress from 'material-ui/CircularProgress';

import Overlay from './overlay';

const style = {
  margin: 'auto',
};

const ProgressLoader = ({ show }) => (
  <Overlay show={show}>
    <CircularProgress style={style} />
  </Overlay>
);

export default ProgressLoader;
