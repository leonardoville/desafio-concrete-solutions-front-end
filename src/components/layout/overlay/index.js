import React from 'react';
import PropTypes from 'prop-types';

import Transition from 'react-transition-group/Transition';

import './index.css';

const DURATION = 500;

const transitionStyles = {
  entering: { opacity: 0 },
  entered: { opacity: 1 },
  exited: { opacity: 0 },
};

const Overlay = ({ children, show }) => (
  <Transition in={show} timeout={DURATION} unmountOnExit={true}>
    {(state) => <div style={transitionStyles[state]} className="overlay">{children}</div>}
  </Transition>
);

Overlay.propTypes = {
  show: PropTypes.bool.isRequired,
};

export default Overlay;
