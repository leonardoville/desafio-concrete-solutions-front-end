import React from 'react';
import { shallow } from 'enzyme';

import Overlay from './index'

describe('Overlay', () => {
  it('should render with success', () => {
    const wrapper = shallow(<Overlay show={true}/>);

    expect(wrapper).toMatchSnapshot();
  });
});
