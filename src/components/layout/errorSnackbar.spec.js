import React from 'react';
import { shallow } from 'enzyme';

import ErrorSnackbar from './errorSnackbar';

describe('ErrorSnackbar', () => {
  it('should render with success', () => {
    const error = { message: 'error' };
    const clearErrorMock = jest.fn();
    const wrapper = shallow(<ErrorSnackbar error={error} clearError={clearErrorMock} />);

    expect(wrapper).toMatchSnapshot();
  });
});
