import React from 'react';
import { shallow } from 'enzyme';

import { ContentDisplayer } from './index';

it('should render ContentDisplayer with props title with success', () => {
  const ChildrenStub = () => <div />;
  const wrapper = shallow(<ContentDisplayer pageTitle="Teste"><ChildrenStub/></ContentDisplayer>);

  expect(wrapper).toMatchSnapshot();
});
