import React from 'react';
import PropTypes from 'prop-types';

import { Card, CardTitle, CardText } from 'material-ui';

const style = {
  width: '85%',
  margin: '0 auto',
  marginTop: '2.5%',
};

export const ContentDisplayer = ({ pageTitle, children }) => (
  <Card style={style}>
    <CardTitle title={pageTitle}/>
    <CardText>
      {children}
    </CardText>
  </Card>
);

ContentDisplayer.propTypes = {
  pageTitle: PropTypes.string,
};

export default ContentDisplayer;
