import React from 'react';
import Chance from 'chance';
import { shallow, mount } from 'enzyme';

import { Layout } from './index';
import ContentDisplayer from './content';
import ErrorSnackbar from './errorSnackbar';

jest.mock('react-dom', () => ({ 'createPortal': jest.fn() }));
jest.mock('../../modules/user', () => ({
  find: { containers: { SearchUser: () => <div /> } }
}));

describe('Layout', () => {
  let chance;

  beforeAll(() => {
    chance = new Chance();
  });

  it('should render layout with content displayer when have children passed', () => {
    const ChildrenStub = () => <div />;
    const wrapper = shallow(
      <Layout loading={chance.bool()} clearError={jest.fn()} pageTitle={chance.word()}>
        <ChildrenStub />
      </Layout>
    );

    expect(wrapper.find(ContentDisplayer).exists()).toBe(true);
  });

  it('should render layout without content displayer when not have children passed', () => {
    const wrapper = shallow(<Layout loading={chance.bool()} clearError={jest.fn()} pageTitle={chance.word()} />);

    expect(wrapper.find(ContentDisplayer).exists()).toBe(false);
  });

  it('should render ErrorSnackbar component when error prop is defined and pass correct props to it', () => {
    const errorStub = { message: chance.sentence() };
    const clearErrorMock = jest.fn();
    const wrapper = mount(
      <Layout loading={chance.bool()} clearError={clearErrorMock} pageTitle={chance.word()} error={errorStub} />
    );

    expect(wrapper.find(ErrorSnackbar).exists()).toBe(true);
    expect(wrapper.find(ErrorSnackbar).prop('error')).toBe(errorStub);
    expect(wrapper.find(ErrorSnackbar).prop('clearError')).toBe(clearErrorMock);
  });

  it('should not render ErrorSnackbar component when error prop is not defined', () => {
    const wrapper = shallow(<Layout clearError={jest.fn} loading={chance.bool()} pageTitle={chance.word()} />);

    expect(wrapper.find(ErrorSnackbar).exists()).toBe(false);
  });

  it('should render Layout component with correct structure', () => {
    const wrapper = shallow(<Layout loading={false} clearError={jest.fn()} pageTitle="teste" />);

    expect(wrapper).toMatchSnapshot();
  });
});
