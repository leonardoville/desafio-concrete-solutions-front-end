import React from 'react';

import Snackbar from 'material-ui/Snackbar';

const ErrorSnackbar = ({ error, clearError }) => (
  <Snackbar
    open={true}
    message={error.message}
    autoHideDuration={4000}
    onRequestClose={clearError}
  />
);

export default ErrorSnackbar;
