import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import { isEmpty } from 'lodash';

import { global } from '../../modules';

import NavBar from '../navbar';
import ContentDisplayer from './content';
import ErrorSnackbar from './errorSnackbar';
import ProgressLoader from './progressLoader';

const renderLayoutContent = (children, pageTitle) => (
  children
    ? <ContentDisplayer pageTitle={pageTitle}>{children}</ContentDisplayer>
    : <div/>
);

export const Layout = ({ children, loading, pageTitle, error, clearError }) => (
  <MuiThemeProvider>
    <React.Fragment>
      <NavBar />
      {ReactDOM.createPortal(<ProgressLoader show={loading}/>, document.body)}
      {renderLayoutContent(children, pageTitle)}
      {isEmpty(error) ? null : <ErrorSnackbar error={error} clearError={clearError} />}
    </React.Fragment>
  </MuiThemeProvider>
);

Layout.propTypes = {
  pageTitle: PropTypes.string,
  loading: PropTypes.bool.isRequired,
  clearError: PropTypes.func.isRequired,
  error: PropTypes.shape({
    message: PropTypes.string,
  }),
};

function mapStateToProps(state, ownProps) {
  return {
    loading: state.global.fetchingData,
    pageTitle: state.global.currentPageTitle,
    error: state.global.error,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ clearError: global.actions.clearError }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
