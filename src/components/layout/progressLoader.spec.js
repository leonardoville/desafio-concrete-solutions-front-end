import React from 'react';
import { shallow } from 'enzyme';

import ProgressLoader from './progressLoader';

describe('ProgressLoader', () => {
  it('should render with success', () => {
    const showStub = true;
    const wrapper = shallow(<ProgressLoader show={showStub} />);

    expect(wrapper).toMatchSnapshot();
  });
});
