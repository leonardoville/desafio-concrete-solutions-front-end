import React from 'react';
import { shallow } from 'enzyme';

import App from './app';

jest.mock('./components/layout', () => jest.fn());

describe('App', () => {
  it('should render with success', () => {
    const historyStub = {};
    const wrapper = shallow(<App history={historyStub}/>);

    expect(wrapper).toMatchSnapshot();
  });
});
