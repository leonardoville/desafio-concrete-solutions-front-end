import { createStore } from 'redux';

import rootReducer from './rootReducer';

import { global, user, repository } from '../modules';

describe('RootReducer', () => {
  it('should create rootReducer with keys defined in constants of each reducer module and same initial state', () => {
    let store = createStore(rootReducer);

    expect(store.getState()[user.constants.NAME]).toEqual(user.reducer(undefined, {}));
    expect(store.getState()[global.constants.NAME]).toEqual(global.reducer(undefined, {}));
    expect(store.getState()[repository.constants.NAME]).toEqual(repository.reducer(undefined, {}));
    expect(store.getState().routing).toBeDefined();
  });
});
