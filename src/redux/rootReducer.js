import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import { global, user, repository } from '../modules';

export default combineReducers({
  routing: routerReducer,
  [global.constants.NAME]: global.reducer,
  [user.constants.NAME]: user.reducer,
  [repository.constants.NAME]: repository.reducer,
});
